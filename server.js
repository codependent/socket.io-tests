var SERVER_IP_ADDRESS = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1'
var SERVER_PORT = process.env.OPENSHIFT_NODEJS_PORT || 3000

var path = require('path');
var express = require("express");
var app = express();

console.log("server ip: "+SERVER_IP_ADDRESS);
console.log("server port: "+SERVER_PORT);

//Enables CORS
var enableCORS = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With, *');
 
        // intercept OPTIONS method
    if ('OPTIONS' == req.method) {
        res.send(200);
    } else {
        next();
    };
};

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(enableCORS);
app.use(express.static(__dirname + "/public"));

app.get('/', function(req, res){
	console.log("/");
	var port = process.env.OPENSHIFT_NODEJS_PORT ? "" : ":"+SERVER_PORT;
	res.render('index', { title: 'Socket-IO Test', address : "http://"+ SERVER_IP_ADDRESS + port});
});

server = require("http").createServer(app),
io = require("socket.io").listen(server);
io.set( 'origins', '*' );
io.sockets.on("connection", function(socket) {
	socket.emit("ping");
	socket.on("pong", function() {
		console.log("PONG!");
	});
});

server.listen(SERVER_PORT, SERVER_IP_ADDRESS, function () {
	console.log( "Listening on " + SERVER_IP_ADDRESS + ", server_port " + SERVER_PORT )
});